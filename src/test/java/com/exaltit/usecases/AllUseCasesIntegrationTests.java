package com.exaltit.usecases;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.operation.AccountOperation;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
class AllUseCasesIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void verify_that_a_full_workflow_is_functional() throws Exception {

        // Bank Account Creation --> balance : 0
        final MvcResult result = this.mockMvc.perform(post("/accounts/")).andDo(print()).andDo(document("/accounts/")).andExpect(status().isOk())
                                             .andReturn();
        final BankAccount openedBankAccount = objectMapper.readValue(
                result.getResponse().getContentAsString(),
                BankAccount.class
        );
        assertNotNull(openedBankAccount);
        final var bankAccountId = openedBankAccount.getAccountId();

        // Make a withdrawal money on the newly empty Bank Account --> balance : 0
        this.mockMvc.perform(post("/accounts/withdrawal/{accountId}/{amount}", bankAccountId, 150)).andDo(print())
                    .andExpect(status().isConflict());

        // Make a deposit on the Bank Account --> balance : 150
        this.mockMvc.perform(post("/accounts/deposit/{accountId}/{amount}", bankAccountId, 150)).andDo(print())
                    .andExpect(status().isOk());

        // Make a deposit on the Bank Account --> balance : 350
        this.mockMvc.perform(post("/accounts/deposit/{accountId}/{amount}", bankAccountId, 200)).andDo(print())
                    .andExpect(status().isOk());

        // Make a deposit on the Bank Account --> balance : 500
        this.mockMvc.perform(post("/accounts/deposit/{accountId}/{amount}", bankAccountId, 150)).andDo(print())
                    .andExpect(status().isOk());

        // Make a withdrawal money on the newly empty Bank Account --> balance : 350
        this.mockMvc.perform(post("/accounts/withdrawal/{accountId}/{amount}", bankAccountId, 150)).andDo(print())
                    .andExpect(status().isOk());

        // Make a withdrawal money on the newly empty Bank Account --> balance : 200
        this.mockMvc.perform(post("/accounts/withdrawal/{accountId}/{amount}", bankAccountId, 150)).andDo(print())
                    .andExpect(status().isOk());

        // Make a withdrawal money on the newly empty Bank Account --> balance : 50
        this.mockMvc.perform(post("/accounts/withdrawal/{accountId}/{amount}", bankAccountId, 150)).andDo(print())
                    .andExpect(status().isOk());

        // Make a withdrawal money on the newly empty Bank Account --> balance : 0
        this.mockMvc.perform(post("/accounts/withdrawal/{accountId}/{amount}", bankAccountId, 50)).andDo(print())
                    .andExpect(status().isOk());

        // Check the Operations Account (0 = all)
        this.mockMvc
                .perform(get("/accounts/check/{accountId}/{operationNumber}", bankAccountId, 0)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(8)))
                .andExpect(jsonPath("$[0].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[0].amount").value(50))
                .andExpect(jsonPath("$[0].balance").value(0))
                .andExpect(jsonPath("$[1].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[1].amount").value(150))
                .andExpect(jsonPath("$[1].balance").value(50))
                .andExpect(jsonPath("$[2].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[2].amount").value(150))
                .andExpect(jsonPath("$[2].balance").value(200))
                .andExpect(jsonPath("$[3].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[3].amount").value(150))
                .andExpect(jsonPath("$[3].balance").value(350))
                .andExpect(jsonPath("$[4].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[4].amount").value(150))
                .andExpect(jsonPath("$[4].balance").value(500))
                .andExpect(jsonPath("$[5].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[5].amount").value(200))
                .andExpect(jsonPath("$[5].balance").value(350))
                .andExpect(jsonPath("$[6].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[6].amount").value(150))
                .andExpect(jsonPath("$[6].balance").value(150))
                .andExpect(jsonPath("$[7].operationType").value(AccountOperation.OPERATION_TYPE_CREATION))
                .andExpect(jsonPath("$[7].amount").value(0))
                .andExpect(jsonPath("$[7].balance").value(0));

        // Check the Operations Account (10 operations to display : more than the stored operations list size)
        this.mockMvc
                .perform(get("/accounts/check/{accountId}/{operationNumber}", bankAccountId, 0)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(8)))
                .andExpect(jsonPath("$[0].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[0].amount").value(50))
                .andExpect(jsonPath("$[0].balance").value(0))
                .andExpect(jsonPath("$[1].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[1].amount").value(150))
                .andExpect(jsonPath("$[1].balance").value(50))
                .andExpect(jsonPath("$[2].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[2].amount").value(150))
                .andExpect(jsonPath("$[2].balance").value(200))
                .andExpect(jsonPath("$[3].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[3].amount").value(150))
                .andExpect(jsonPath("$[3].balance").value(350))
                .andExpect(jsonPath("$[4].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[4].amount").value(150))
                .andExpect(jsonPath("$[4].balance").value(500))
                .andExpect(jsonPath("$[5].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[5].amount").value(200))
                .andExpect(jsonPath("$[5].balance").value(350))
                .andExpect(jsonPath("$[6].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[6].amount").value(150))
                .andExpect(jsonPath("$[6].balance").value(150))
                .andExpect(jsonPath("$[7].operationType").value(AccountOperation.OPERATION_TYPE_CREATION))
                .andExpect(jsonPath("$[7].amount").value(0))
                .andExpect(jsonPath("$[7].balance").value(0));

        // Check the Operations Account (7 operations to display : less than the stored operations list size)
        this.mockMvc
                .perform(get("/accounts/check/{accountId}/{operationNumber}", bankAccountId, 7)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(7)))
                .andExpect(jsonPath("$[0].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[0].amount").value(50))
                .andExpect(jsonPath("$[0].balance").value(0))
                .andExpect(jsonPath("$[1].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[1].amount").value(150))
                .andExpect(jsonPath("$[1].balance").value(50))
                .andExpect(jsonPath("$[2].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[2].amount").value(150))
                .andExpect(jsonPath("$[2].balance").value(200))
                .andExpect(jsonPath("$[3].operationType").value(AccountOperation.OPERATION_TYPE_WITHDRAWAL))
                .andExpect(jsonPath("$[3].amount").value(150))
                .andExpect(jsonPath("$[3].balance").value(350))
                .andExpect(jsonPath("$[4].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[4].amount").value(150))
                .andExpect(jsonPath("$[4].balance").value(500))
                .andExpect(jsonPath("$[5].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[5].amount").value(200))
                .andExpect(jsonPath("$[5].balance").value(350))
                .andExpect(jsonPath("$[6].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[6].amount").value(150))
                .andExpect(jsonPath("$[6].balance").value(150));
    }
}
