package com.exaltit.application.adapter;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.operation.AccountOperation;
import com.exaltit.domain.port.BankAccountProviderPort;
import com.exaltit.infrastructure.adapter.AccountEntity;
import com.exaltit.infrastructure.adapter.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
class BankAccountControllerAdapterIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    BankAccountProviderPort bankAccountProviderPort;

    @Test
    void deposit_should_return_ok_and_increase_the_balance_account_with_the_deposit_amount() throws Exception {
        final String accountId = "1111";
        accountRepository.save(AccountEntity.builder().accountId(accountId).balance(1000).operations(Collections.emptyList()).build());

        this.mockMvc.perform(post("/accounts/deposit/{accountId}/{amount}", accountId, 150)).andDo(print())
                    .andExpect(status().isOk()).andDo(document("/accounts/deposit/"));

        assertEquals(1150, accountRepository.findByAccountId(accountId).getBalance());
    }

    @Test
    void deposit_should_return_a_NOT_FOUND_due_to_unexisting_account() throws Exception {

        this.mockMvc.perform(post("/accounts/deposit/{accountId}/{amount}", "unknown", 150)).andDo(print())
                    .andExpect(status().isNotFound()).andDo(document("/accounts/deposit/errors/unexisting-account"));
    }

    @Test
    void deposit_should_return_bad_request_due_to_negative_deposit_amount() throws Exception {
        final String accountId = "2222";
        accountRepository.save(AccountEntity.builder().accountId(accountId).balance(1000).operations(Collections.emptyList()).build());

        this.mockMvc.perform(post("/accounts/deposit/{accountId}/{amount}", accountId, -150)).andDo(print())
                    .andExpect(status().isBadRequest()).andDo(document("/accounts/deposit/errors/negative-deposit"));
    }

    @Test
    void deposit_should_return_bad_request_due_to_0_deposit_amount() throws Exception {
        final String accountId = "3333";
        accountRepository.save(AccountEntity.builder().accountId(accountId).balance(1000).operations(Collections.emptyList()).build());

        this.mockMvc.perform(post("/accounts/deposit/{accountId}/{amount}", accountId, 0)).andDo(print())
                    .andExpect(status().isNotModified()).andDo(document("/accounts/deposit/errors/0-deposit"));
    }

    @Test
    void withdrawal_should_return_ok_and_decrease_the_balance_account_with_the_withdrawal_amount()
    throws Exception {
        final String accountId = "usecase2-1111";
        accountRepository.save(AccountEntity.builder().accountId(accountId).balance(1000).operations(Collections.emptyList()).build());

        this.mockMvc.perform(post("/accounts/withdrawal/{accountId}/{amount}", accountId, 150)).andDo(print())
                    .andExpect(status().isOk()).andDo(document("/accounts/withdrawal/"));

        assertEquals(850, accountRepository.findByAccountId(accountId).getBalance());
    }

    @Test
    void withdrawal_should_return_bad_request_due_to_unexisting_account() throws Exception {

        this.mockMvc.perform(post("/accounts/withdrawal/{accountId}/{amount}", "unknown", 150)).andDo(print())
                    .andExpect(status().isNotFound()).andDo(document("/accounts/withdrawal/errors/unexisting-account"));
    }

    @Test
    void withdrawal_should_return_bad_request_due_to_0_withdrawal_amount() throws Exception {
        final String accountId = "usecase2-2222";
        accountRepository.save(AccountEntity.builder().accountId(accountId).balance(1000).operations(Collections.emptyList()).build());

        this.mockMvc.perform(post("/accounts/withdrawal/{accountId}/{amount}", accountId, 0)).andDo(print())
                    .andExpect(status().isNotModified()).andDo(document("/accounts/withdrawal/errors/0-withdrawal"));
    }

    @Test
    void withdrawal_should_return_bad_request_due_to_negative_withdrawal_amount() throws Exception {
        final String accountId = "usecase2-3333";
        accountRepository.save(AccountEntity.builder().accountId(accountId).balance(1000).operations(Collections.emptyList()).build());

        this.mockMvc.perform(post("/accounts/withdrawal/{accountId}/{amount}", accountId, -20)).andDo(print())
                    .andExpect(status().isBadRequest()).andDo(document("/accounts/withdrawal/errors/negative-withdrawal"));
    }

    @Test
    void withdrawal_should_return_conflict_http_error_due_to_amount_greater_than_the_current_bank_account_balance()
    throws Exception {
        final String accountId = "usecase2-4444";
        accountRepository.save(AccountEntity.builder().accountId(accountId).balance(1000).operations(Collections.emptyList()).build());

        this.mockMvc.perform(post("/accounts/withdrawal/{accountId}/{amount}", accountId, 2000)).andDo(print())
                    .andExpect(status().isConflict()).andDo(document("/accounts/withdrawal/errors/not-enough-money"));

        assertEquals(1000, accountRepository.findByAccountId(accountId).getBalance());
    }

    @Test
    void check_account_operations_should_return_a_list_with_correct_size_of_operations() throws Exception {
        final String accountId = "usecase3-111";
        final Date currentDate = new Date();
        final Date nextCurrentDate = new Date(currentDate.getTime() + 10);

        final AccountOperation operation1 = new AccountOperation(AccountOperation.OPERATION_TYPE_CREATION, currentDate, 0,
                0
        );
        final AccountOperation operation2 = new AccountOperation(AccountOperation.OPERATION_TYPE_DEPOSIT, nextCurrentDate, 10,
                10
        );
        final List<AccountOperation> operations = new ArrayList<>();
        operations.add(operation2);
        operations.add(operation1);

        final BankAccount bankAccountWithOperations = BankAccount.builder().accountId(accountId).accountBalance(0).operations(operations).build();

        bankAccountProviderPort.create(bankAccountWithOperations);

        this.mockMvc
                .perform(get("/accounts/check/{accountId}/{operationNumber}", accountId, 1)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[0].amount").value(10))
                .andExpect(jsonPath("$[0].balance").value(10))
                .andDo(document("/accounts/check/"));
        this.mockMvc
                .perform(get("/accounts/check/{accountId}/{operationNumber}", accountId, 2)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[1].operationType").value(AccountOperation.OPERATION_TYPE_CREATION))
                .andExpect(jsonPath("$[1].amount").value(0))
                .andExpect(jsonPath("$[1].balance").value(0))
                .andExpect(jsonPath("$[0].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[0].amount").value(10))
                .andExpect(jsonPath("$[0].balance").value(10));
        this.mockMvc
                .perform(get("/accounts/check/{accountId}/{operationNumber}", accountId, 3)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[1].operationType").value(AccountOperation.OPERATION_TYPE_CREATION))
                .andExpect(jsonPath("$[1].amount").value(0))
                .andExpect(jsonPath("$[1].balance").value(0))
                .andExpect(jsonPath("$[0].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[0].amount").value(10))
                .andExpect(jsonPath("$[0].balance").value(10));
        this.mockMvc
                .perform(get("/accounts/check/{accountId}/{operationNumber}", accountId, 0)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[1].operationType").value(AccountOperation.OPERATION_TYPE_CREATION))
                .andExpect(jsonPath("$[1].amount").value(0))
                .andExpect(jsonPath("$[1].balance").value(0))
                .andExpect(jsonPath("$[0].operationType").value(AccountOperation.OPERATION_TYPE_DEPOSIT))
                .andExpect(jsonPath("$[0].amount").value(10))
                .andExpect(jsonPath("$[0].balance").value(10));
    }

    @Test
    void check_account_operations_should_return_a_NOT_FOUND_when_the_request_is_targeting_an_unknown_account()
    throws Exception {
        final String unknownAccountId = "UnknownAccountId";
        this.mockMvc.perform(get("/accounts/check/{accountId}/{operationNumber}", unknownAccountId, 1)).andDo(print())
                    .andExpect(status().isNotFound()).andDo(document("/accounts/check/errors/unexisting-account"));
    }

    @Test
    void check_account_operations_should_return_a_BAD_REQUEST_when_the_request_is_requesting__negative_number_of_operations()
    throws Exception {
        this.mockMvc.perform(get("/accounts/check/{accountId}/{operationNumber}", "accountId", -13)).andDo(print())
                    .andExpect(status().isBadRequest()).andDo(document("/accounts/check/errors/negative-operation-number"));
    }
}
