package com.exaltit;

import org.junit.jupiter.api.Test;

// Test class added ONLY to cover main() invocation not covered by application tests.
class TestingWebApplication {

    @Test
    void main() {
        BankAccountApplication.main(new String[]{});
    }
}
