package com.exaltit.deposit;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.account.exceptions.ZeroAmountValueProvidedException;
import com.exaltit.domain.deposit.MakeADepositService;
import com.exaltit.domain.deposit.MakeADepositUseCase;
import com.exaltit.domain.operation.AccountOperation;
import com.exaltit.domain.operation.AddAccountOperationService;
import com.exaltit.domain.operation.AddAccountOperationUseCase;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;
import com.exaltit.domain.port.BankAccountProviderPort;
import com.exaltit.infrastructure.adapter.BankAccountProviderH2Adapter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class that will test the make a deposit Use case
 *
 * @author Tomiche
 */
class DepositBankAccountTest {

    private final BankAccountProviderPort bankAccountProviderPort = Mockito.mock(BankAccountProviderH2Adapter.class);
    private final AddAccountOperationUseCase addAccountOperationUseCase = new AddAccountOperationService(bankAccountProviderPort);
    private final MakeADepositUseCase makeADepositUseCase = new MakeADepositService(
            bankAccountProviderPort,
            addAccountOperationUseCase
    );

    @Test
    void make_a_deposit_when_account_exists_and_is_empty_must_return_a_BankAccount_with_the_deposit_amount_added_to_its_account_balance()
    throws UnknownBankAccountException, NullBankAccountIdProvidedException, NegativeNumberProvidedException,
            ZeroAmountValueProvidedException, NullAccountOperationProvidedException {
        final BankAccount bankAccount = BankAccount.builder().accountId("TestAccount").accountBalance(0).operations(new ArrayList<>()).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId()))
               .thenReturn(Optional.of(bankAccount));

        final BankAccount updatedBankAccount = makeADepositUseCase.makeADeposit(bankAccount.getAccountId(), 1000);
        assertEquals(1000, updatedBankAccount.getAccountBalance());
        assertEquals(bankAccount.getAccountId(), updatedBankAccount.getAccountId());
    }

    @Test
    void make_a_deposit_when_account_is_null_must_throw_a_null_bank_account_id_provided_exception() {
        final BankAccount bankAccount = BankAccount.builder().accountId(null).accountBalance(0).build();
        Assertions.assertThrows(NullBankAccountIdProvidedException.class, () -> {
            // Code under test
            makeADepositUseCase.makeADeposit(bankAccount.getAccountId(), 1000);
        });
    }

    @Test
    void make_a_deposit_when_account_is_unknown_must_throw_an_unknown_bank_account_exception() {
        Mockito.when(bankAccountProviderPort.getBankAccountById("UnknownTestAccount")).thenReturn(Optional.empty());

        Assertions.assertThrows(UnknownBankAccountException.class, () -> {
            // Code under test
            makeADepositUseCase.makeADeposit("UnknownTestAccount", 1000);
        });
    }

    @Test
    void make_a_negative_deposit_must_throw_a_negative_number_provided_exception_and_not_update_the_account_balance() {
        final BankAccount bankAccount = BankAccount.builder().accountId("TestAccount").accountBalance(0).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId()))
               .thenReturn(Optional.of(bankAccount));
        Assertions.assertThrows(NegativeNumberProvidedException.class, () -> {
            // Code under test
            makeADepositUseCase.makeADeposit(bankAccount.getAccountId(), -1);
        });
        assertEquals(0, bankAccount.getAccountBalance());
    }

    @Test
    void make_an_empty_deposit_must_throw_a_zero_amount_value_provided_exception_and_not_update_the_account_balance() {
        final BankAccount bankAccount = BankAccount.builder().accountId("TestAccount").accountBalance(0).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId()))
               .thenReturn(Optional.of(bankAccount));
        Assertions.assertThrows(ZeroAmountValueProvidedException.class, () -> {
            // Code under test
            makeADepositUseCase.makeADeposit(bankAccount.getAccountId(), 0);
        });

        assertEquals(0, bankAccount.getAccountBalance());
    }

    @Test
    void make_a_deposit_when_account_exists_should_add_an_operation_to_the_bank_account()
    throws UnknownBankAccountException, NullBankAccountIdProvidedException, NegativeNumberProvidedException,
            ZeroAmountValueProvidedException, NullAccountOperationProvidedException {
        final BankAccount bankAccount = BankAccount.builder().accountId("TestAccount").accountBalance(10).operations(new ArrayList<>()).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId()))
               .thenReturn(Optional.of(bankAccount));

        final BankAccount updatedBankAccount = makeADepositUseCase.makeADeposit(bankAccount.getAccountId(), 1000);
        assertEquals(1010, updatedBankAccount.getAccountBalance());
        assertEquals(1, updatedBankAccount.getOperations().size());
        final AccountOperation lastOperation = updatedBankAccount.getOperations().get(updatedBankAccount.getOperations().size() - 1);
        assertEquals(AccountOperation.OPERATION_TYPE_DEPOSIT, lastOperation.getOperationType());
        assertEquals(1010, lastOperation.getBalance());
        assertEquals(1000, lastOperation.getAmount());
    }
}
