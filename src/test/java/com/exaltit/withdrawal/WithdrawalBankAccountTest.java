package com.exaltit.withdrawal;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.account.exceptions.ZeroAmountValueProvidedException;
import com.exaltit.domain.operation.AccountOperation;
import com.exaltit.domain.operation.AddAccountOperationService;
import com.exaltit.domain.operation.AddAccountOperationUseCase;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;
import com.exaltit.domain.port.BankAccountProviderPort;
import com.exaltit.domain.withdrawal.MakeAWithdrawalService;
import com.exaltit.domain.withdrawal.MakeAWithdrawalUseCase;
import com.exaltit.domain.withdrawal.exceptions.NotEnoughMoneyToWithdrawException;
import com.exaltit.infrastructure.adapter.BankAccountProviderH2Adapter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class that will test the make a withdrawal Use case
 *
 * @author Tomiche
 */
class WithdrawalBankAccountTest {

    private final BankAccountProviderPort bankAccountProviderPort = Mockito.mock(BankAccountProviderH2Adapter.class);
    private final AddAccountOperationUseCase addAccountOperationUseCase = new AddAccountOperationService(bankAccountProviderPort);

    private final MakeAWithdrawalUseCase makeAWithdrawalUseCase = new MakeAWithdrawalService(
            bankAccountProviderPort,
            addAccountOperationUseCase
    );

    @Test
    void make_a_withdrawal_when_account_exists_and_has_enough_money_on_it_must_return_a_BankAccount_with_the_withdrawal_amount_removed_from_the_account_balance()
    throws NotEnoughMoneyToWithdrawException, NullBankAccountIdProvidedException, UnknownBankAccountException,
            NegativeNumberProvidedException, ZeroAmountValueProvidedException, NullAccountOperationProvidedException {
        final BankAccount bankAccount = BankAccount.builder().accountId("TestAccount").accountBalance(1000).operations(new ArrayList<>()).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId()))
               .thenReturn(Optional.of(bankAccount));

        final BankAccount updatedBankAccount = makeAWithdrawalUseCase.makeAWithdrawal(bankAccount.getAccountId(), 100);
        assertEquals(900, updatedBankAccount.getAccountBalance());
        assertEquals(bankAccount.getAccountId(), updatedBankAccount.getAccountId());
    }

    @Test
    void make_a_withdrawal_when_account_is_null_must_throw_a_null_bank_account_id_provided_exception() {
        final BankAccount bankAccount = BankAccount.builder().accountId(null).accountBalance(1000).build();

        Assertions.assertThrows(NullBankAccountIdProvidedException.class, () -> {
            // Code under test
            makeAWithdrawalUseCase.makeAWithdrawal(bankAccount.getAccountId(), 100);
        });
    }

    @Test
    void make_a_withdrawal_when_account_is_unknown_must_throw_an_unknown_bank_account_exception() {
        Mockito.when(bankAccountProviderPort.getBankAccountById("UnknownTestAccount")).thenReturn(Optional.empty());

        Assertions.assertThrows(UnknownBankAccountException.class, () -> {
            // Code under test
            makeAWithdrawalUseCase.makeAWithdrawal("UnknownTestAccount", 100);
        });
    }

    @Test
    void make_a_negative_withdrawal_must_throw_a_negative_number_provided_exception_and_not_update_the_account_balance() {
        final BankAccount bankAccount = BankAccount.builder().accountId("TestAccount").accountBalance(1000).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId()))
               .thenReturn(Optional.of(bankAccount));

        Assertions.assertThrows(NegativeNumberProvidedException.class, () -> {
            // Code under test
            makeAWithdrawalUseCase.makeAWithdrawal(bankAccount.getAccountId(), -1);
        });
        assertEquals(1000, bankAccount.getAccountBalance());
    }

    @Test
    void make_an_empty_withdrawal_must_throw_a_zero_amount_value_provided_exception_and_not_update_the_account_balance() {
        final BankAccount bankAccount = BankAccount.builder().accountId("TestAccount").accountBalance(1000).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId()))
               .thenReturn(Optional.of(bankAccount));

        Assertions.assertThrows(ZeroAmountValueProvidedException.class, () -> {
            // Code under test
            makeAWithdrawalUseCase.makeAWithdrawal(bankAccount.getAccountId(), 0);
        });
        assertEquals(1000, bankAccount.getAccountBalance());
    }

    @Test
    void make_a_withdrawal_greater_than_the_account_balance_must_return_throw_an_exception_and_not_update_the_account_balance() {
        final BankAccount bankAccount = BankAccount.builder().accountId("TestAccount").accountBalance(500).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId()))
               .thenReturn(Optional.of(bankAccount));
        Assertions.assertThrows(NotEnoughMoneyToWithdrawException.class, () -> {
            // Code under test
            makeAWithdrawalUseCase.makeAWithdrawal(bankAccount.getAccountId(), 600);
        });
        assertEquals(500, bankAccount.getAccountBalance());
    }

    @Test
    void make_a_withdrawal_when_account_exists_should_add_an_operation_to_the_bank_account()
    throws NotEnoughMoneyToWithdrawException, NullBankAccountIdProvidedException, UnknownBankAccountException,
            NegativeNumberProvidedException, ZeroAmountValueProvidedException, NullAccountOperationProvidedException {
        final BankAccount bankAccount = BankAccount.builder().accountId("TestAccount").accountBalance(1000).operations(new ArrayList<>()).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId()))
               .thenReturn(Optional.of(bankAccount));

        final BankAccount updatedBankAccount = makeAWithdrawalUseCase.makeAWithdrawal(bankAccount.getAccountId(), 10);
        assertEquals(990, updatedBankAccount.getAccountBalance());
        assertEquals(1, updatedBankAccount.getOperations().size());
        final AccountOperation lastOperation = updatedBankAccount.getOperations().get(updatedBankAccount.getOperations().size() - 1);
        assertEquals(AccountOperation.OPERATION_TYPE_WITHDRAWAL, lastOperation.getOperationType());
        assertEquals(990, lastOperation.getBalance());
        assertEquals(10, lastOperation.getAmount());
    }
}
