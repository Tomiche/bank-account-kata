package com.exaltit.account;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.OpenBankAccountService;
import com.exaltit.domain.account.OpenBankAccountUseCase;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.operation.AccountOperation;
import com.exaltit.domain.operation.AddAccountOperationService;
import com.exaltit.domain.operation.AddAccountOperationUseCase;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;
import com.exaltit.domain.port.BankAccountProviderPort;
import com.exaltit.infrastructure.adapter.BankAccountProviderH2Adapter;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class OpenBankAccountTest {

    private final BankAccountProviderPort bankAccountProviderPort = Mockito.mock(BankAccountProviderH2Adapter.class);
    private final AddAccountOperationUseCase addAccountOperationUseCase = Mockito
            .mock(AddAccountOperationService.class);
    private final OpenBankAccountUseCase openBankAccountUseCase = new OpenBankAccountService(
            bankAccountProviderPort,
            addAccountOperationUseCase
    );

    @Test
    void check_open_account_returns_the_newly_created_account_id() throws NullBankAccountIdProvidedException,
            NullAccountOperationProvidedException, UnknownBankAccountException {
        final BankAccount account = openBankAccountUseCase.openBankAccount();

        assertNotNull(account);
    }

    @Test
    void open_an_account_should_add_an_operation_to_the_bank_account() throws NullBankAccountIdProvidedException,
            NullAccountOperationProvidedException, UnknownBankAccountException {
        openBankAccountUseCase.openBankAccount();

        verify(addAccountOperationUseCase, times(1)).addAccountOperation(anyString(), any(AccountOperation.class));
    }
}
