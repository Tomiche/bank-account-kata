package com.exaltit.operation;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.operation.AccountOperation;
import com.exaltit.domain.operation.AddAccountOperationService;
import com.exaltit.domain.operation.AddAccountOperationUseCase;
import com.exaltit.domain.operation.CheckAccountOperationsService;
import com.exaltit.domain.operation.CheckAccountOperationsUseCase;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;
import com.exaltit.domain.port.BankAccountProviderPort;
import com.exaltit.infrastructure.adapter.BankAccountProviderH2Adapter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AddOperationBankAccountTest {

    private final BankAccountProviderPort bankAccountProviderPort = Mockito.mock(BankAccountProviderH2Adapter.class);
    private final CheckAccountOperationsUseCase checkAccountOperationsUseCase = new CheckAccountOperationsService(
            bankAccountProviderPort);
    private final AddAccountOperationUseCase addAccountOperationUseCase = new AddAccountOperationService(
            bankAccountProviderPort);

    @Test
    void check_add_account_operations_onto_an_existing_bank_account_returns_true_and_store_the_operation()
    throws UnknownBankAccountException, NullBankAccountIdProvidedException, NegativeNumberProvidedException,
            NullAccountOperationProvidedException {
        final Date currentDate = new Date();
        final Date nextCurrentDate = new Date(currentDate.getTime() + 10);

        final AccountOperation operation1 = new AccountOperation(AccountOperation.OPERATION_TYPE_CREATION, currentDate, 10,
                10
        );

        final List<AccountOperation> operations = new ArrayList<>();
        operations.add(operation1);
        final BankAccount bankAccountWithOperations = BankAccount.builder().accountId("TestAccount").accountBalance(0).operations(operations).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccountWithOperations.getAccountId()))
               .thenReturn(Optional.of(bankAccountWithOperations));

        List<AccountOperation> resultList = checkAccountOperationsUseCase
                .checkAccountOperations(bankAccountWithOperations.getAccountId(), 2);
        assertFalse(resultList.isEmpty());
        assertEquals(1, resultList.size());

        // Add new Operation
        final AccountOperation operation2 = new AccountOperation(AccountOperation.OPERATION_TYPE_DEPOSIT, nextCurrentDate, 10,
                20
        );

        final boolean result = addAccountOperationUseCase.addAccountOperation(bankAccountWithOperations.getAccountId(), operation2);
        resultList = checkAccountOperationsUseCase.checkAccountOperations(bankAccountWithOperations.getAccountId(), 2);
        assertTrue(result);
        assertEquals(2, resultList.size());
        assertEquals(AccountOperation.OPERATION_TYPE_DEPOSIT, resultList.get(0).getOperationType());
        assertEquals(AccountOperation.OPERATION_TYPE_CREATION, resultList.get(1).getOperationType());
    }

    @Test
    void check_add_account_operations_when_account_is_unknown_must_throw_an_unknown_bank_account_exception() {
        final BankAccount bankAccount = BankAccount.builder().accountId("UnknownTestAccount").accountBalance(0).build();
        final Date currentDate = new Date();
        final AccountOperation operation1 = new AccountOperation(AccountOperation.OPERATION_TYPE_CREATION, currentDate, 10,
                10
        );
        Assertions.assertThrows(
                UnknownBankAccountException.class,
                () -> addAccountOperationUseCase.addAccountOperation(bankAccount.getAccountId(), operation1)
        );
    }

    @Test
    void check_add_account_operations_when_account_is_null_must_throw_a_null_bank_account_id_provided_exception() {
        final BankAccount bankAccount = BankAccount.builder().accountId(null).accountBalance(0).build();
        final Date currentDate = new Date();
        final AccountOperation operation1 = new AccountOperation(AccountOperation.OPERATION_TYPE_CREATION, currentDate, 10,
                10
        );

        Assertions.assertThrows(NullBankAccountIdProvidedException.class, () -> {
            // Code under test
            addAccountOperationUseCase.addAccountOperation(bankAccount.getAccountId(), operation1);
        });
    }

    @Test
    void check_add_a_null_account_operation_onto_an_existing_bank_account_must_throw_a_null_account_operation_provided_exception() throws
            UnknownBankAccountException,
            NullBankAccountIdProvidedException,
            NegativeNumberProvidedException {
        final BankAccount bankAccount = BankAccount.builder().accountId("TestAccount").accountBalance(0).operations(new ArrayList<>()).build();

        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId()))
               .thenReturn(Optional.of(bankAccount));

        List<AccountOperation> resultList = checkAccountOperationsUseCase
                .checkAccountOperations(bankAccount.getAccountId(), 2);
        assertTrue(resultList.isEmpty());

        // Add new Operation
        final AccountOperation operation1 = null;

        Assertions.assertThrows(NullAccountOperationProvidedException.class, () -> {
            // Code under test
            addAccountOperationUseCase.addAccountOperation(bankAccount.getAccountId(), operation1);
        });
        resultList = checkAccountOperationsUseCase.checkAccountOperations(bankAccount.getAccountId(), 2);
        assertTrue(resultList.isEmpty());
    }

}
