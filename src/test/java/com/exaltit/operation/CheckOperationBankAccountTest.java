package com.exaltit.operation;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.operation.AccountOperation;
import com.exaltit.domain.operation.CheckAccountOperationsService;
import com.exaltit.domain.operation.CheckAccountOperationsUseCase;
import com.exaltit.domain.port.BankAccountProviderPort;
import com.exaltit.infrastructure.adapter.BankAccountProviderH2Adapter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CheckOperationBankAccountTest {

    private final BankAccountProviderPort bankAccountProviderPort = Mockito.mock(BankAccountProviderH2Adapter.class);
    private final CheckAccountOperationsUseCase checkAccountOperationsUseCase = new CheckAccountOperationsService(
            bankAccountProviderPort);

    @Test
    void check_account_operations_should_return_a_list_of_operations() throws
            UnknownBankAccountException,
            NullBankAccountIdProvidedException,
            NegativeNumberProvidedException {
        final Date currentDate = new Date();
        final Date nextCurrentDate = new Date(currentDate.getTime() + 10);

        final AccountOperation operation1 = new AccountOperation(AccountOperation.OPERATION_TYPE_CREATION, currentDate, 10,
                10
        );
        final AccountOperation operation2 = new AccountOperation(AccountOperation.OPERATION_TYPE_DEPOSIT, nextCurrentDate, 10,
                20
        );
        final List<AccountOperation> operations = new ArrayList<>();
        operations.add(operation1);
        operations.add(operation2);

        final BankAccount bankAccountWithOperations = BankAccount.builder().accountId("TestAccount").accountBalance(0).operations(operations).build();

        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccountWithOperations.getAccountId()))
               .thenReturn(Optional.of(bankAccountWithOperations));

        final List<AccountOperation> result = checkAccountOperationsUseCase.checkAccountOperations(
                bankAccountWithOperations.getAccountId(),
                2
        );
        assertFalse(result.isEmpty());
        assertEquals(2, result.size());
    }

    @Test
    void check_account_operations_should_return_an_empty_list_of_operations_if_no_operations_are_found() throws
            UnknownBankAccountException,
            NullBankAccountIdProvidedException,
            NegativeNumberProvidedException {
        final BankAccount bankAccount = BankAccount.builder().accountId("TestAccount").accountBalance(0).operations(new ArrayList<>()).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId()))
               .thenReturn(Optional.of(bankAccount));

        final List<AccountOperation> result = checkAccountOperationsUseCase.checkAccountOperations(
                bankAccount.getAccountId(),
                2
        );
        assertTrue(result.isEmpty());
    }

    @Test
    void check_account_operations_should_throw_an_unknwon_bank_account_exception_when_account_is_not_known() {
        final BankAccount bankAccount = BankAccount.builder().accountId("UnknownTestAccount").accountBalance(0).build();
        Assertions.assertThrows(UnknownBankAccountException.class, () -> {
            // Code under test
            checkAccountOperationsUseCase.checkAccountOperations(bankAccount.getAccountId(), 2);
        });
    }

    @Test
    void check_account_operations_should_throw_a_null_bank_account_id_provided_exception_when_account_is_null() {
        final BankAccount bankAccount = BankAccount.builder().accountId(null).accountBalance(0).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccount.getAccountId())).thenReturn(null);

        Assertions.assertThrows(NullBankAccountIdProvidedException.class, () -> {
            // Code under test
            checkAccountOperationsUseCase.checkAccountOperations(bankAccount.getAccountId(), 2);
        });
    }

    @Test
    void check_account_operations_should_return_a_list_of_operations_with_a_specified_item_number() throws
            UnknownBankAccountException,
            NullBankAccountIdProvidedException,
            NegativeNumberProvidedException {
        final Date currentDate = new Date();
        final Date operation2CurrentDate = new Date(currentDate.getTime() + 10);
        final Date operation3CurrentDate = new Date(currentDate.getTime() + 20);

        final AccountOperation operation1 = new AccountOperation(AccountOperation.OPERATION_TYPE_CREATION, currentDate, 10,
                10
        );
        final AccountOperation operation2 = new AccountOperation(AccountOperation.OPERATION_TYPE_DEPOSIT,
                operation2CurrentDate, 10, 20
        );
        final AccountOperation operation3 = new AccountOperation(AccountOperation.OPERATION_TYPE_WITHDRAWAL,
                operation3CurrentDate, 30, 0
        );
        final List<AccountOperation> operations = new ArrayList<>();
        operations.add(operation1);
        operations.add(operation2);
        operations.add(operation3);

        final BankAccount bankAccountWithOperations = BankAccount.builder().accountId("TestAccount").accountBalance(0).operations(operations).build();

        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccountWithOperations.getAccountId()))
               .thenReturn(Optional.of(bankAccountWithOperations));

        final List<AccountOperation> result = checkAccountOperationsUseCase.checkAccountOperations(
                bankAccountWithOperations.getAccountId(),
                2
        );
        assertFalse(result.isEmpty());
        assertEquals(2, result.size());
    }

    @Test
    void check_account_operations_should_return_a_list_of_operations_with_a_specified_item_number_or_the_maximum_operation_number() throws
            UnknownBankAccountException,
            NullBankAccountIdProvidedException,
            NegativeNumberProvidedException {
        final Date currentDate = new Date();
        final Date operation2CurrentDate = new Date(currentDate.getTime() + 10);
        final Date operation3CurrentDate = new Date(currentDate.getTime() + 20);

        final AccountOperation operation1 = new AccountOperation(AccountOperation.OPERATION_TYPE_CREATION, currentDate, 10,
                10
        );
        final AccountOperation operation2 = new AccountOperation(AccountOperation.OPERATION_TYPE_DEPOSIT,
                operation2CurrentDate, 10, 20
        );
        final AccountOperation operation3 = new AccountOperation(AccountOperation.OPERATION_TYPE_WITHDRAWAL,
                operation3CurrentDate, 30, 0
        );
        final List<AccountOperation> operations = new ArrayList<>();
        operations.add(operation1);
        operations.add(operation2);
        operations.add(operation3);

        final BankAccount bankAccountWithOperations = BankAccount.builder().accountId("TestAccount").accountBalance(0).operations(operations).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccountWithOperations.getAccountId()))
               .thenReturn(Optional.of(bankAccountWithOperations));

        final List<AccountOperation> result = checkAccountOperationsUseCase.checkAccountOperations(
                bankAccountWithOperations.getAccountId(),
                4
        );
        assertFalse(result.isEmpty());
        assertEquals(3, result.size());
    }

    @Test
    void check_account_operations_should_return_the_full_list_of_operations_when_operation_number_is_set_to_0() throws
            UnknownBankAccountException,
            NullBankAccountIdProvidedException,
            NegativeNumberProvidedException {
        final Date currentDate = new Date();
        final Date operation2CurrentDate = new Date(currentDate.getTime() + 10);
        final Date operation3CurrentDate = new Date(currentDate.getTime() + 20);

        final AccountOperation operation1 = new AccountOperation(AccountOperation.OPERATION_TYPE_CREATION, currentDate, 10,
                10
        );
        final AccountOperation operation2 = new AccountOperation(AccountOperation.OPERATION_TYPE_DEPOSIT,
                operation2CurrentDate, 10, 20
        );
        final AccountOperation operation3 = new AccountOperation(AccountOperation.OPERATION_TYPE_WITHDRAWAL,
                operation3CurrentDate, 30, 0
        );
        final List<AccountOperation> operations = new ArrayList<>();
        operations.add(operation1);
        operations.add(operation2);
        operations.add(operation3);

        final BankAccount bankAccountWithOperations = BankAccount.builder().accountId("TestAccount").accountBalance(0).operations(operations).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccountWithOperations.getAccountId()))
               .thenReturn(Optional.of(bankAccountWithOperations));

        final List<AccountOperation> result = checkAccountOperationsUseCase.checkAccountOperations(
                bankAccountWithOperations.getAccountId(),
                0
        );
        assertFalse(result.isEmpty());
        assertEquals(operations.size(), result.size());
    }

    @Test
    void check_account_operations_should_throw_a_negative_number_provided_exception_when_operation_number_is_negative() {
        final Date currentDate = new Date();
        final Date operation2CurrentDate = new Date(currentDate.getTime() + 10);
        final Date operation3CurrentDate = new Date(currentDate.getTime() + 20);

        final AccountOperation operation1 = new AccountOperation(AccountOperation.OPERATION_TYPE_CREATION, currentDate, 10,
                10
        );
        final AccountOperation operation2 = new AccountOperation(AccountOperation.OPERATION_TYPE_DEPOSIT,
                operation2CurrentDate, 10, 20
        );
        final AccountOperation operation3 = new AccountOperation(AccountOperation.OPERATION_TYPE_WITHDRAWAL,
                operation3CurrentDate, 30, 0
        );
        final List<AccountOperation> operations = new ArrayList<>();
        operations.add(operation1);
        operations.add(operation2);
        operations.add(operation3);

        final BankAccount bankAccountWithOperations = BankAccount.builder().accountId("TestAccount").accountBalance(0).operations(operations).build();
        Mockito.when(bankAccountProviderPort.getBankAccountById(bankAccountWithOperations.getAccountId()))
               .thenReturn(Optional.of(bankAccountWithOperations));

        Assertions.assertThrows(NegativeNumberProvidedException.class, () -> {
            // Code under test
            checkAccountOperationsUseCase.checkAccountOperations(bankAccountWithOperations.getAccountId(), -1);
        });
    }
}
