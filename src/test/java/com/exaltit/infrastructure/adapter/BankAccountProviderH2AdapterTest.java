package com.exaltit.infrastructure.adapter;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.OpenBankAccountService;
import com.exaltit.domain.operation.AccountOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class BankAccountProviderH2AdapterTest {

    @Autowired
    private AccountRepository accountRepository;

    private BankAccountProviderH2Adapter bankAccountProviderH2Adapter;

    @BeforeEach
    public void setUp() {
        bankAccountProviderH2Adapter = new BankAccountProviderH2Adapter(accountRepository);
    }

    @Test
    void check_that_entity_is_well_stored_and_can_be_got_from_the_H2_adapter() {
        accountRepository.save(AccountEntity.builder().accountId("test").balance(150).operations(Collections.emptyList()).build());
        accountRepository.flush();

        final Optional<BankAccount> existingAccount = bankAccountProviderH2Adapter.getBankAccountById("test");
        final Optional<BankAccount> unexistingAccount = bankAccountProviderH2Adapter.getBankAccountById("testunknown");

        assertTrue(existingAccount.isPresent());
        assertEquals(150, existingAccount.get().getAccountBalance());

        assertTrue(unexistingAccount.isEmpty());
    }

    @Test
    void check_that_entity_is_well_stored_and_can_be_got_and_updated_from_the_H2_adapter() {
        accountRepository.save(AccountEntity.builder().accountId("test2").balance(150).operations(Collections.emptyList()).build());
        accountRepository.flush();

        final Optional<BankAccount> existingAccount = bankAccountProviderH2Adapter.getBankAccountById("test2");

        assertTrue(existingAccount.isPresent());
        final BankAccount bankAccount = existingAccount.get();
        assertEquals(150, bankAccount.getAccountBalance());

        // Update the Bank Account
        final BankAccount updatedBalanceAccount = bankAccount.addAmountToAccountBalance(150);
        bankAccountProviderH2Adapter.update(updatedBalanceAccount);

        final Optional<BankAccount> existingAccountAfterUpdate = bankAccountProviderH2Adapter.getBankAccountById("test2");
        assertTrue(existingAccountAfterUpdate.isPresent());
        final BankAccount bankAccountAfterUpdate = existingAccountAfterUpdate.get();
        assertEquals(300, bankAccountAfterUpdate.getAccountBalance());
    }

    @Test
    void check_that_a_new_account_entity_contains_an_empty_list_of_operations() {
        final String accountId = "test3";
        accountRepository.save(AccountEntity.builder().accountId(accountId).balance(150).operations(Collections.emptyList()).build());
        accountRepository.flush();

        final Optional<BankAccount> existingAccount = bankAccountProviderH2Adapter.getBankAccountById(accountId);

        assertTrue(existingAccount.isPresent());
        final BankAccount bankAccount = existingAccount.get();
        assertEquals(150, bankAccount.getAccountBalance());
        assertTrue(bankAccount.getOperations().isEmpty());
    }

    @Test
    void check_that_an_account_entity_with_account_operations_contain_a_not_empty_list_of_operations() {
        final String accountId = "test4";

        final Date currentDate = new Date();
        final Date nextCurrentDate = new Date(currentDate.getTime() + 10);

        final AccountEntityOperation operation1 = new AccountEntityOperation(AccountOperation.OPERATION_TYPE_CREATION,
                currentDate, 10, 10
        );
        final AccountEntityOperation operation2 = new AccountEntityOperation(AccountOperation.OPERATION_TYPE_DEPOSIT,
                nextCurrentDate, 10, 20
        );
        final List<AccountEntityOperation> operations = new ArrayList<>();
        operations.add(operation1);
        operations.add(operation2);

        accountRepository.save(AccountEntity.builder().accountId(accountId).balance(150).operations(operations).build());
        accountRepository.flush();

        final Optional<BankAccount> existingAccount = bankAccountProviderH2Adapter.getBankAccountById(accountId);

        assertTrue(existingAccount.isPresent());
        final BankAccount bankAccount = existingAccount.get();
        assertEquals(150, bankAccount.getAccountBalance());
        assertFalse(bankAccount.getOperations().isEmpty());
        assertEquals(operation1.convertToDomain(), bankAccount.getOperations().get(0));
        assertEquals(operation2.convertToDomain(), bankAccount.getOperations().get(1));
    }

    @Test
    void check_that_adding_operations_to_an_account_entity_is_well_stored() {
        final String accountId = "test5";

        final Date currentDate = new Date();
        final Date operation2CurrentDate = new Date(currentDate.getTime() + 10);
        final Date operation3CurrentDate = new Date(currentDate.getTime() + 20);

        final AccountEntityOperation operation1 = new AccountEntityOperation(AccountOperation.OPERATION_TYPE_CREATION,
                currentDate, 10, 10
        );
        final AccountEntityOperation operation2 = new AccountEntityOperation(AccountOperation.OPERATION_TYPE_DEPOSIT,
                operation2CurrentDate, 10, 20
        );
        final List<AccountEntityOperation> operations = new ArrayList<>();
        operations.add(operation1);
        operations.add(operation2);

        accountRepository.save(AccountEntity.builder().accountId(accountId).balance(150).operations(operations).build());
        accountRepository.flush();

        Optional<BankAccount> existingAccount = bankAccountProviderH2Adapter.getBankAccountById(accountId);

        assertTrue(existingAccount.isPresent());
        BankAccount bankAccount = existingAccount.get();
        assertEquals(150, bankAccount.getAccountBalance());
        assertFalse(bankAccount.getOperations().isEmpty());
        assertEquals(operation1.convertToDomain(), bankAccount.getOperations().get(0));
        assertEquals(operation2.convertToDomain(), bankAccount.getOperations().get(1));

        // Update the Bank Account with new Operation
        final AccountEntityOperation operation3 = new AccountEntityOperation(AccountOperation.OPERATION_TYPE_DEPOSIT,
                operation3CurrentDate, 10, 30
        );
        bankAccount.getOperations().add(operation3.convertToDomain());
        bankAccountProviderH2Adapter.update(bankAccount);

        // Verify that operation3 is well stored in the Account Entity
        existingAccount = bankAccountProviderH2Adapter.getBankAccountById(accountId);

        assertTrue(existingAccount.isPresent());
        bankAccount = existingAccount.get();
        assertEquals(operation3.convertToDomain(), bankAccount.getOperations().get(2));
    }

    @Test
    void check_that_a_created_account_is_well_stored() {
        final String accountId = UUID.randomUUID().toString();
        bankAccountProviderH2Adapter.create(BankAccount.builder().accountId(accountId).accountBalance(OpenBankAccountService.OPENING_AMOUNT).build());
        final Optional<BankAccount> existingAccount = bankAccountProviderH2Adapter.getBankAccountById(accountId);
        assertTrue(existingAccount.isPresent());
        final BankAccount bankAccount = existingAccount.get();
        assertEquals(accountId, bankAccount.getAccountId());
        assertEquals(OpenBankAccountService.OPENING_AMOUNT, bankAccount.getAccountBalance());
    }
}
