package com.exaltit.infrastructure.adapter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
class AccountRepositoryIntegrationTest {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    void check_that_entity_is_well_stored_and_can_be_got_from_the_repository() {
        accountRepository.save(AccountEntity.builder().accountId("test").balance(150).operations(Collections.emptyList()).build());

        final AccountEntity entity = accountRepository.findByAccountId("test");

        assertThat(entity).isNotNull();
        assertEquals(150, entity.getBalance());
    }
}
