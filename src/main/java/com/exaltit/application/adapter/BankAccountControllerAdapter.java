package com.exaltit.application.adapter;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.OpenBankAccountUseCase;
import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.account.exceptions.ZeroAmountValueProvidedException;
import com.exaltit.domain.deposit.MakeADepositUseCase;
import com.exaltit.domain.operation.AccountOperation;
import com.exaltit.domain.operation.CheckAccountOperationsUseCase;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;
import com.exaltit.domain.withdrawal.MakeAWithdrawalUseCase;
import com.exaltit.domain.withdrawal.exceptions.NotEnoughMoneyToWithdrawException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller Class that will handle the Bank Account most common operations.
 *
 * @author Tomiche
 */
@RestController
@Slf4j
public final class BankAccountControllerAdapter {

    private final MakeADepositUseCase makeADepositUseCase;
    private final MakeAWithdrawalUseCase makeAWithdrawalUseCase;
    private final CheckAccountOperationsUseCase checkAccountOperationsUseCase;
    private final OpenBankAccountUseCase openBankAccountUseCase;

    public BankAccountControllerAdapter(
            final MakeADepositUseCase makeADepositUseCase,
            final MakeAWithdrawalUseCase makeAWithdrawalUseCase,
            final CheckAccountOperationsUseCase checkAccountOperationsUseCase,
            final OpenBankAccountUseCase openBankAccountUseCase
    ) {
        this.makeADepositUseCase = makeADepositUseCase;
        this.makeAWithdrawalUseCase = makeAWithdrawalUseCase;
        this.checkAccountOperationsUseCase = checkAccountOperationsUseCase;
        this.openBankAccountUseCase = openBankAccountUseCase;
    }

    /**
     * Method used to make a deposit into a Bank Account through a REST API.
     *
     * @param accountId the accountId to put the deposit on
     * @param amount    The amount to deposit
     */
    @Operation(summary = "Make a Deposit into a Bank Account")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Successful deposit onto the BankAccount", content = @Content),
                    @ApiResponse(responseCode = "400", description = "Invalid accountId supplied", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Requested Bank Account does not exist", content = @Content),
                    @ApiResponse(responseCode = "304", description = "A 0 amount will not be taken into account.", content = @Content),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Something went wrong. Please contact your System Administrator.",
                            content = @Content
                    )
            }
    )
    @PostMapping(path = "/accounts/deposit/{accountId}/{amount}")
    ResponseEntity<String> makeADeposit(
            @PathVariable("accountId") final String accountId,
            @PathVariable("amount") final int amount
    ) throws
            NullAccountOperationProvidedException,
            UnknownBankAccountException,
            NegativeNumberProvidedException,
            NullBankAccountIdProvidedException,
            ZeroAmountValueProvidedException {
        log.info("makeADeposit method has been called on {} with an amount of {}", accountId, amount);
        makeADepositUseCase.makeADeposit(accountId, amount);
        return ResponseEntity.ok("Successful deposit onto the Bank Account");
    }

    /**
     * Method used to make a withdrawal from a Bank Account through a REST API.
     *
     * @param accountId the accountId to make the withdrawal from
     * @param amount    The amount to withdraw
     */
    @Operation(summary = "Make a Withdrawal from a Bank Account")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Successful withdrawal from the BankAccount", content = @Content),
                    @ApiResponse(responseCode = "400", description = "Invalid accountId supplied", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Requested Bank Account does not exist", content = @Content),
                    @ApiResponse(responseCode = "304", description = "A 0 amount will not be taken into account.", content = @Content),
                    @ApiResponse(responseCode = "409", description = "Not Enough Money to perform this withdrawal", content = @Content),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Something went wrong. Please contact your System Administrator.",
                            content = @Content
                    )
            }
    )
    @PostMapping(path = "/accounts/withdrawal/{accountId}/{amount}")
    ResponseEntity<String> makeAWithdrawal(
            @PathVariable("accountId") final String accountId,
            @PathVariable("amount") final int amount
    ) throws
            NullAccountOperationProvidedException,
            UnknownBankAccountException,
            NegativeNumberProvidedException,
            NotEnoughMoneyToWithdrawException,
            NullBankAccountIdProvidedException,
            ZeroAmountValueProvidedException {
        log.info("makeAWithdrawal method has been called on {} with an amount of {}", accountId, amount);
        makeAWithdrawalUseCase.makeAWithdrawal(accountId, amount);

        return ResponseEntity.ok("Successful withdrawal from the Bank Account");
    }

    /**
     * Method used to make a withdrawal from a Bank Account through a REST API.
     *
     * @param accountId       the accountId to check the operations from
     * @param operationNumber The operation number to retrieve.
     */
    @Operation(summary = "Check the Operations performed on a Bank Account")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "List of AccountOperation successfully retrieve from the BankAccount",
                            content = @Content
                    ),
                    @ApiResponse(responseCode = "400", description = "Invalid accountId supplied or negative operation number", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Requested Bank Account does not exist", content = @Content)
            }
    )
    @GetMapping(path = "/accounts/check/{accountId}/{operationNumber}")
    ResponseEntity<List<AccountOperation>> checkAccountOperations(
            @PathVariable("accountId") final String accountId,
            @PathVariable("operationNumber") final int operationNumber
    ) throws UnknownBankAccountException, NegativeNumberProvidedException, NullBankAccountIdProvidedException {
        List<AccountOperation> accountOperations;
        accountOperations = checkAccountOperationsUseCase.checkAccountOperations(accountId, operationNumber);

        return ResponseEntity.ok(accountOperations);
    }

    /**
     * Method used to open a Bank Account through a REST API.
     */
    @Operation(summary = "Open a Bank Account")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "List of AccountOperation successfully retrieve from the BankAccount",
                            content = @Content
                    ),
                    @ApiResponse(responseCode = "400", description = "Invalid accountId returned from database", content = @Content),
                    @ApiResponse(responseCode = "404", description = "Requested Bank Account does not exist", content = @Content),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Something went wrong. Please contact your System Administrator.",
                            content = @Content
                    )
            }
    )
    @PostMapping(path = "/accounts/")
    ResponseEntity<BankAccount> openBankAccount() throws
            NullAccountOperationProvidedException,
            UnknownBankAccountException,
            NullBankAccountIdProvidedException {
        BankAccount bankAccount;
        bankAccount = openBankAccountUseCase.openBankAccount();
        return ResponseEntity.ok(bankAccount);
    }
}
