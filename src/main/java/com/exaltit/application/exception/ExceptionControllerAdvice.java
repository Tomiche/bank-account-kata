package com.exaltit.application.exception;

import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.account.exceptions.ZeroAmountValueProvidedException;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;
import com.exaltit.domain.withdrawal.exceptions.NotEnoughMoneyToWithdrawException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@Slf4j
@RestControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(UnknownBankAccountException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorMessage> unknownBankAccountException(UnknownBankAccountException ex) {
        log.error("UnknownBankAccountException: {}", ex.getMessage());
        return new ResponseEntity<>(buildErrorMessage(HttpStatus.NOT_FOUND.value(), ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NullBankAccountIdProvidedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorMessage> nullBankAccountIdProvidedException(NullBankAccountIdProvidedException ex) {
        log.error("NullBankAccountIdProvidedException: {}", ex.getMessage());
        return new ResponseEntity<>(buildErrorMessage(HttpStatus.BAD_REQUEST.value(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NegativeNumberProvidedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorMessage> negativeNumberProvidedException(NegativeNumberProvidedException ex) {
        log.error("NegativeNumberProvidedException: {}", ex.getMessage());
        return new ResponseEntity<>(buildErrorMessage(HttpStatus.BAD_REQUEST.value(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ZeroAmountValueProvidedException.class)
    @ResponseStatus(HttpStatus.NOT_MODIFIED)
    public ResponseEntity<String> zeroAmountValueProvidedException(ZeroAmountValueProvidedException ex) {
        log.error("ZeroAmountValueProvidedException: {}", ex.getMessage());
        return new ResponseEntity<>("A 0 amount will not be taken into account.", HttpStatus.NOT_MODIFIED);
    }

    @ExceptionHandler(NullAccountOperationProvidedException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<String> nullAccountOperationProvidedException(NullAccountOperationProvidedException ex) {
        log.error("NullAccountOperationProvidedException: {}", ex.getMessage());
        return new ResponseEntity<>("Something went wrong. Please contact your System Administrator.", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotEnoughMoneyToWithdrawException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<String> notEnoughMoneyToWithdrawException(NotEnoughMoneyToWithdrawException ex) {
        log.error("NotEnoughMoneyToWithdrawException: {}", ex.getMessage());
        return new ResponseEntity<>(
                "You do not have enough money to perform this withdrawal. Please consider a lower withdrawal.",
                HttpStatus.CONFLICT
        );
    }

    private ErrorMessage buildErrorMessage(int statusCode, String message) {
        return new ErrorMessage(statusCode, message, LocalDateTime.now());
    }
}
