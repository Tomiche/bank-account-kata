package com.exaltit.application.exception;

import java.time.LocalDateTime;

public record ErrorMessage(int statusCode,
                           String message,
                           LocalDateTime date){
}
