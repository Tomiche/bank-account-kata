package com.exaltit.domain.withdrawal.exceptions;

import java.io.Serial;

/**
 * Exception Class that should be thrown when there is not enough money in the Bank Account to perform a specific operation.
 *
 * @author Tomiche
 */
public final class NotEnoughMoneyToWithdrawException extends Exception {

    private static final String THERE_IS_NOT_ENOUGH_MONEY_IN_YOUR_BANK_ACCOUNT_TO_WITHDRAW_SO_MUCH_MONEY =
            "There is not enough money in your Bank Account to withdraw so much money.";

    /**
     * Generated serial version UID.
     */
    @Serial
    private static final long serialVersionUID = 2311915079036023741L;

    /**
     * Constructor using errorMessage as parameter.
     */
    public NotEnoughMoneyToWithdrawException() {
        super(THERE_IS_NOT_ENOUGH_MONEY_IN_YOUR_BANK_ACCOUNT_TO_WITHDRAW_SO_MUCH_MONEY);
    }
}
