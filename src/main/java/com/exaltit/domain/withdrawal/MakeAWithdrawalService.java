package com.exaltit.domain.withdrawal;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.account.exceptions.ZeroAmountValueProvidedException;
import com.exaltit.domain.operation.AccountOperation;
import com.exaltit.domain.operation.AddAccountOperationUseCase;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;
import com.exaltit.domain.port.BankAccountProviderPort;
import com.exaltit.domain.withdrawal.exceptions.NotEnoughMoneyToWithdrawException;
import lombok.RequiredArgsConstructor;

import java.util.Date;

/**
 * Class that implements the MakeAWithdrawalUseCase Interface.
 *
 * @author Tomiche
 */
@RequiredArgsConstructor
public final class MakeAWithdrawalService implements MakeAWithdrawalUseCase {

    final BankAccountProviderPort bankAccountProviderPort;
    final AddAccountOperationUseCase addAccountOperationUseCase;

    @Override
    public BankAccount makeAWithdrawal(final String accountId, final int amount)
    throws NotEnoughMoneyToWithdrawException, NullBankAccountIdProvidedException, UnknownBankAccountException,
            NegativeNumberProvidedException, ZeroAmountValueProvidedException, NullAccountOperationProvidedException {
        validateInputs(accountId, amount);
        BankAccount account = bankAccountProviderPort.getBankAccountById(accountId).orElseThrow(UnknownBankAccountException::new);
        if (account.getAccountBalance() < amount) {
            throw new NotEnoughMoneyToWithdrawException();
        }
        final BankAccount updatedBalanceAccount = account.removeAmountFromAccountBalance(amount);
        bankAccountProviderPort.update(updatedBalanceAccount);
        addAccountOperationUseCase.addAccountOperation(accountId, new AccountOperation(
                AccountOperation.OPERATION_TYPE_WITHDRAWAL, new Date(), amount, updatedBalanceAccount.getAccountBalance()));
        return updatedBalanceAccount;
    }

    private void validateInputs(final String accountId, final int amount) throws NullBankAccountIdProvidedException,
            NegativeNumberProvidedException, ZeroAmountValueProvidedException {
        if (null == accountId) {
            throw new NullBankAccountIdProvidedException();
        }
        if (amount < 0) {
            throw new NegativeNumberProvidedException();
        }
        if (amount == 0) {
            throw new ZeroAmountValueProvidedException();
        }
    }

}
