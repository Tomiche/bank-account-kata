package com.exaltit.domain.withdrawal;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.account.exceptions.ZeroAmountValueProvidedException;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;
import com.exaltit.domain.withdrawal.exceptions.NotEnoughMoneyToWithdrawException;

/**
 * Interface that must be implemented in order to make a withdrawal onto the BankAccount.
 *
 * @author Tomiche
 */
public interface MakeAWithdrawalUseCase {

    /**
     * Method used to make a withdrawal onto the BankAccount.
     *
     * @param accountId The account Id to make the withdrawal from
     * @param amount    the amount to withdraw from the BankAccount.
     * @return The BankAccount if the withdrawal went well
     * @throws NotEnoughMoneyToWithdrawException     When there is not enough money onto the Bank Account to make a withdrawal
     * @throws NullBankAccountIdProvidedException    When the BankAccount Id provided is null
     * @throws UnknownBankAccountException           When the requested Bank Account does not exist
     * @throws NegativeNumberProvidedException       When the withdrawal amount is negative
     * @throws ZeroAmountValueProvidedException      When the Deposit amount is zero
     * @throws NullAccountOperationProvidedException When the generated Account Operation is null
     */
    BankAccount makeAWithdrawal(String accountId, int amount)
    throws NotEnoughMoneyToWithdrawException, NullBankAccountIdProvidedException, UnknownBankAccountException,
            NegativeNumberProvidedException, ZeroAmountValueProvidedException, NullAccountOperationProvidedException;

}
