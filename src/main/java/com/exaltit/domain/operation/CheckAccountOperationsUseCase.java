package com.exaltit.domain.operation;

import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;

import java.util.List;

/**
 * Interface that must be implemented in order to check operations performed onto the BankAccount.
 *
 * @author Tomiche
 */
public interface CheckAccountOperationsUseCase {

    /**
     * Method used to check the latest operations onto the BankAccount.
     *
     * @param accountId       The account Id to make the withdrawal from
     * @param operationNumber The number of operations to get onto the BankAccount - If 0, return the full list.
     * @return The list of the latest n (= operationNumber) operations performed onto the BankAccount
     * @throws UnknownBankAccountException        When the requested Bank Account does not exist
     * @throws NullBankAccountIdProvidedException When the BankAccount Id provided is null
     * @throws NegativeNumberProvidedException    When the Operation Number provided is negative
     */
    List<AccountOperation> checkAccountOperations(String accountId, int operationNumber)
    throws UnknownBankAccountException, NullBankAccountIdProvidedException, NegativeNumberProvidedException;
}
