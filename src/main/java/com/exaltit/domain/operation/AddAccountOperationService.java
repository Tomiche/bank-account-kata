package com.exaltit.domain.operation;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;
import com.exaltit.domain.port.BankAccountProviderPort;
import lombok.RequiredArgsConstructor;

/**
 * Class that implements the AddAccountOperationUseCase Interface.
 *
 * @author Tomiche
 */
@RequiredArgsConstructor
public final class AddAccountOperationService implements AddAccountOperationUseCase {

    private final BankAccountProviderPort bankAccountProviderPort;

    @Override
    public boolean addAccountOperation(final String accountId, final AccountOperation operation)
    throws NullBankAccountIdProvidedException, NullAccountOperationProvidedException,
            UnknownBankAccountException {
        validateInputs(accountId, operation);

        BankAccount account = bankAccountProviderPort.getBankAccountById(accountId).orElseThrow(UnknownBankAccountException::new);
        account.addOperation(operation);
        bankAccountProviderPort.update(account);
        return true;
    }

    private void validateInputs(final String accountId, final AccountOperation operation)
    throws NullBankAccountIdProvidedException, NullAccountOperationProvidedException {
        if (null == accountId) {
            throw new NullBankAccountIdProvidedException();
        }
        if (null == operation) {
            throw new NullAccountOperationProvidedException();
        }
    }
}
