package com.exaltit.domain.operation;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Date;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class AccountOperation {

    public static final String OPERATION_TYPE_DEPOSIT = "Deposit";
    public static final String OPERATION_TYPE_WITHDRAWAL = "Withdrawal";
    public static final String OPERATION_TYPE_CREATION = "Creation";

    String operationType;
    Date operationDate;
    int amount;
    float balance;
}
