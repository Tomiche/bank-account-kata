package com.exaltit.domain.operation;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.port.BankAccountProviderPort;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public final class CheckAccountOperationsService implements CheckAccountOperationsUseCase {

    private final BankAccountProviderPort bankAccountProviderPort;

    @Override
    public List<AccountOperation> checkAccountOperations(final String accountId, final int operationNumber)
    throws UnknownBankAccountException, NullBankAccountIdProvidedException, NegativeNumberProvidedException {
        validateInputs(accountId, operationNumber);
        BankAccount account = bankAccountProviderPort.getBankAccountById(accountId).orElseThrow(UnknownBankAccountException::new);

        if (account.getOperations().isEmpty()) {
            return account.getOperations();
        }
        final int numberOfOperationsToReturn = account.getOperations().size() <= operationNumber || operationNumber == 0
                                               ? account.getOperations().size()
                                               : operationNumber;
        return account.getOperations().subList(0, numberOfOperationsToReturn);
    }

    private void validateInputs(final String accountId, final int operationNumber)
    throws NullBankAccountIdProvidedException, NegativeNumberProvidedException {
        if (null == accountId) {
            throw new NullBankAccountIdProvidedException();
        }
        if (operationNumber < 0) {
            throw new NegativeNumberProvidedException();
        }
    }
}
