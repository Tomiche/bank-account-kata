package com.exaltit.domain.operation.exceptions;

import java.io.Serial;

public final class NullAccountOperationProvidedException extends Exception {

    private static final String THIS_ACCOUNT_OPERATION_IS_NULL = "The provided Account Operation should not be null";

    /**
     * Generated serial version UID.
     */
    @Serial
    private static final long serialVersionUID = 8484614124594428809L;

    /**
     * Constructor using errorMessage as parameter.
     */
    public NullAccountOperationProvidedException() {
        super(THIS_ACCOUNT_OPERATION_IS_NULL);
    }

}
