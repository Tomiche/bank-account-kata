package com.exaltit.domain.operation;

import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;

/**
 * Interface that must be implemented in order to add operations onto the BankAccount.
 *
 * @author Tomiche
 */
public interface AddAccountOperationUseCase {

    /**
     * Method used to add an operation to a Bank Account
     *
     * @param accountId The account ID to add the Operation to
     * @param operation The Operation to add to the Bank Account
     * @return True if Add is Ok, else False
     * @throws NullBankAccountIdProvidedException    When the BankAccount Id provided is null
     * @throws NullAccountOperationProvidedException When the provided Account Operation is null
     * @throws UnknownBankAccountException           When the requested Bank Account does not exist
     */
    boolean addAccountOperation(String accountId, AccountOperation operation) throws
            NullBankAccountIdProvidedException,
            NullAccountOperationProvidedException,
            UnknownBankAccountException;
}
