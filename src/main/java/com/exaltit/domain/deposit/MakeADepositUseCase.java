package com.exaltit.domain.deposit;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.account.exceptions.ZeroAmountValueProvidedException;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;

/**
 * Interface that must be implemented in order to make a deposit onto the BankAccount.
 *
 * @author Tomiche
 */
public interface MakeADepositUseCase {

    /**
     * Method used to make a deposit onto the BankAccount.
     *
     * @param accountId The account Id to make the deposit to
     * @param amount    the amount to put onto the BankAccount.
     * @return The Bank Account if the deposit went well
     * @throws UnknownBankAccountException           When the requested Bank Account does not exist
     * @throws NullBankAccountIdProvidedException    When the BankAccount Id provided is null
     * @throws NegativeNumberProvidedException       When the deposit amount is negative
     * @throws ZeroAmountValueProvidedException      When the Deposit amount is zero
     * @throws NullAccountOperationProvidedException When the generated Account Operation is null
     */
    BankAccount makeADeposit(String accountId, int amount) throws
            UnknownBankAccountException,
            NullBankAccountIdProvidedException,
            NegativeNumberProvidedException,
            ZeroAmountValueProvidedException,
            NullAccountOperationProvidedException;

}
