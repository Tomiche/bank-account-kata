package com.exaltit.domain.deposit;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.account.exceptions.NegativeNumberProvidedException;
import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.account.exceptions.ZeroAmountValueProvidedException;
import com.exaltit.domain.operation.AccountOperation;
import com.exaltit.domain.operation.AddAccountOperationUseCase;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;
import com.exaltit.domain.port.BankAccountProviderPort;
import lombok.RequiredArgsConstructor;

import java.util.Date;

/**
 * Class that implements the MakeADepositUseCase Interface.
 *
 * @author Tomiche
 */
@RequiredArgsConstructor
public final class MakeADepositService implements MakeADepositUseCase {

    private final BankAccountProviderPort bankAccountProviderPort;
    private final AddAccountOperationUseCase addAccountOperationUseCase;

    @Override
    public BankAccount makeADeposit(final String accountId, final int amount)
    throws UnknownBankAccountException, NullBankAccountIdProvidedException, NegativeNumberProvidedException,
            ZeroAmountValueProvidedException, NullAccountOperationProvidedException {
        validateInputs(accountId, amount);
        BankAccount account = bankAccountProviderPort.getBankAccountById(accountId).orElseThrow(UnknownBankAccountException::new);
        final BankAccount updatedBalanceAccount = account.addAmountToAccountBalance(amount);
        bankAccountProviderPort.update(updatedBalanceAccount);
        addAccountOperationUseCase.addAccountOperation(accountId, new AccountOperation(
                AccountOperation.OPERATION_TYPE_DEPOSIT, new Date(), amount, updatedBalanceAccount.getAccountBalance()));
        return updatedBalanceAccount;
    }

    private void validateInputs(final String accountId, final int amount) throws NullBankAccountIdProvidedException,
            NegativeNumberProvidedException, ZeroAmountValueProvidedException {
        if (null == accountId) {
            throw new NullBankAccountIdProvidedException();
        }
        if (amount < 0) {
            throw new NegativeNumberProvidedException();
        }
        if (amount == 0) {
            throw new ZeroAmountValueProvidedException();
        }
    }
}
