package com.exaltit.domain.account;

import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;

/**
 * Interface that must be implemented in order to open a BankAccount
 *
 * @author Tomiche
 */
public interface OpenBankAccountUseCase {

    /**
     * Method to use to open a BankAccount
     *
     * @return The BankAccount
     * @throws NullBankAccountIdProvidedException    When the BankAccount Id returned from the database is null
     * @throws NullAccountOperationProvidedException When the generated Account Operation is null
     * @throws UnknownBankAccountException           When the generated Bank Account does not exist
     */
    BankAccount openBankAccount() throws NullBankAccountIdProvidedException, NullAccountOperationProvidedException, UnknownBankAccountException;
}
