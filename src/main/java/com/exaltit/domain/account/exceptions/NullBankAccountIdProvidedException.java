package com.exaltit.domain.account.exceptions;

import java.io.Serial;

public final class NullBankAccountIdProvidedException extends Exception {

    private static final String THIS_BANK_ACCOUNT_ID_IS_NULL = "The requested Bank Account ID should not be null";

    /**
     * Generated serial version UID.
     */
    @Serial
    private static final long serialVersionUID = -5597197313095009778L;

    /**
     * Constructor using errorMessage as parameter.
     */
    public NullBankAccountIdProvidedException() {
        super(THIS_BANK_ACCOUNT_ID_IS_NULL);
    }

}
