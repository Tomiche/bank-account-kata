package com.exaltit.domain.account.exceptions;

import java.io.Serial;

public final class UnknownBankAccountException extends Exception {

    private static final String THIS_BANK_ACCOUNT_IS_UNKNOWN = "The requested Bank Account does not exist..";

    /**
     * Generated serial version UID.
     */
    @Serial
    private static final long serialVersionUID = 5334222108422269048L;

    /**
     * Constructor using errorMessage as parameter.
     */
    public UnknownBankAccountException() {
        super(THIS_BANK_ACCOUNT_IS_UNKNOWN);
    }

}
