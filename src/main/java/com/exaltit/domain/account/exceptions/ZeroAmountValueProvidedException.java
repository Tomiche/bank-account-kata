package com.exaltit.domain.account.exceptions;

import java.io.Serial;

public final class ZeroAmountValueProvidedException extends Exception {

    private static final String ZERO_AMOUNT_NUMBER_PROVIDED = "A 0 amount value will not be taken into account";

    /**
     * Generated serial version UID.
     */
    @Serial
    private static final long serialVersionUID = 6676599009836852626L;

    /**
     * Constructor using errorMessage as parameter.
     */
    public ZeroAmountValueProvidedException() {
        super(ZERO_AMOUNT_NUMBER_PROVIDED);
    }

}
