package com.exaltit.domain.account.exceptions;

import java.io.Serial;

public final class NegativeNumberProvidedException extends Exception {

    private static final String NEGATIVE_NUMBER_PROVIDED = "The provided parameter number should not be negative.";

    /**
     * Generated serial version UID.
     */
    @Serial
    private static final long serialVersionUID = 7931570768041792375L;

    /**
     * Constructor using errorMessage as parameter.
     */
    public NegativeNumberProvidedException() {
        super(NEGATIVE_NUMBER_PROVIDED);
    }

}
