package com.exaltit.domain.account;

import com.exaltit.domain.operation.AccountOperation;
import lombok.Builder;
import lombok.Value;

import java.util.List;

/**
 * Class that represents a Bank Account.
 *
 * @author Tomiche
 */
@Value
@Builder(toBuilder = true)
public class BankAccount {

    String accountId;
    float accountBalance;
    List<AccountOperation> operations;

    /**
     * Add a specific amount to the account balance
     *
     * @param amount The amount to add to the account balance.
     */
    public BankAccount addAmountToAccountBalance(final int amount) {
        return this.toBuilder().accountBalance(this.accountBalance + amount).build();
    }

    /**
     * Remove a specific amount from the account balance
     *
     * @param amount The amount to remove from the account balance.
     */
    public BankAccount removeAmountFromAccountBalance(final int amount) {
        return this.toBuilder().accountBalance(this.accountBalance - amount).build();
    }

    /**
     * Add an Operation into the bank account.
     *
     * @param operation The operation to add
     */
    public void addOperation(final AccountOperation operation) {
        this.operations.add(0, operation);
    }
}
