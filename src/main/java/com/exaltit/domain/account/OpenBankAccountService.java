package com.exaltit.domain.account;

import com.exaltit.domain.account.exceptions.NullBankAccountIdProvidedException;
import com.exaltit.domain.account.exceptions.UnknownBankAccountException;
import com.exaltit.domain.operation.AccountOperation;
import com.exaltit.domain.operation.AddAccountOperationUseCase;
import com.exaltit.domain.operation.exceptions.NullAccountOperationProvidedException;
import com.exaltit.domain.port.BankAccountProviderPort;
import lombok.RequiredArgsConstructor;

import java.util.Date;
import java.util.UUID;

/**
 * Class that implements the OpenBankAccountUseCase Interface.
 *
 * @author Tomiche
 */
@RequiredArgsConstructor
public final class OpenBankAccountService implements OpenBankAccountUseCase {

    public static final int OPENING_AMOUNT = 0;
    private final BankAccountProviderPort bankAccountProviderPort;
    private final AddAccountOperationUseCase addAccountOperationUseCase;

    @Override
    public BankAccount openBankAccount() throws NullBankAccountIdProvidedException,
            NullAccountOperationProvidedException, UnknownBankAccountException {
        final BankAccount bankAccount = BankAccount.builder().accountId(UUID.randomUUID().toString()).accountBalance(OPENING_AMOUNT).build();
        bankAccountProviderPort.create(bankAccount);
        addAccountOperationUseCase.addAccountOperation(bankAccount.getAccountId(), new AccountOperation(
                AccountOperation.OPERATION_TYPE_CREATION, new Date(), OPENING_AMOUNT, OPENING_AMOUNT));
        return bankAccount;
    }
}
