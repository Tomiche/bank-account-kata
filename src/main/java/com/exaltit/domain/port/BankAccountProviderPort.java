package com.exaltit.domain.port;

import com.exaltit.domain.account.BankAccount;

import java.util.Optional;

public interface BankAccountProviderPort {

    /**
     * Get the Bank Account associated to this account ID from an external source .
     *
     * @param accountId The ID of the Bank Account to get
     * @return An Optional Bank Account
     */
    Optional<BankAccount> getBankAccountById(String accountId);

    /**
     * Update BankAccount with the latests changes perfomed on it.
     *
     * @param bankAccount the bankAccount to save
     */
    void update(BankAccount bankAccount);

    /**
     * Create BankAccount.
     *
     * @param bankAccount the bankAccount to create
     */
    void create(BankAccount bankAccount);

}
