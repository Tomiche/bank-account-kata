package com.exaltit.infrastructure.adapter;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.port.BankAccountProviderPort;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

/**
 * H2 Adapter implementation Class of the BankAccountProviderPort
 *
 * @author Tomiche
 */
@RequiredArgsConstructor
public final class BankAccountProviderH2Adapter implements BankAccountProviderPort {

    private final AccountRepository accountRepository;

    @Override
    public Optional<BankAccount> getBankAccountById(final String accountId) {
        final AccountEntity account = accountRepository.findByAccountId(accountId);
        return null == account ? Optional.empty() : Optional.of(account.convertToDomain());
    }

    @Override
    public void update(final BankAccount bankAccount) {
        final AccountEntity entity = this.accountRepository.findByAccountId(bankAccount.getAccountId());
        this.accountRepository.saveAndFlush(new AccountEntity(entity.getId(), entity.getAccountId(),
                bankAccount.getAccountBalance(), bankAccount.getOperations().stream().map(AccountEntityOperation::convertFromDomain).toList()
        ));
    }

    @Override
    public void create(final BankAccount bankAccount) {
        final Optional<AccountEntity> accountEntity = AccountEntity.convertFromDomain(bankAccount);
        accountEntity.ifPresent(this.accountRepository::saveAndFlush);
    }
}
