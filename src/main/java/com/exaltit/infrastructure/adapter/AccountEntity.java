package com.exaltit.infrastructure.adapter;

import com.exaltit.domain.account.BankAccount;
import com.exaltit.domain.operation.AccountOperation;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Entity Class that will represent a Bank Account
 *
 * @author Tomiche
 */
@Data
@Entity
@Table(name = "account")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public final class AccountEntity {

    @Id
    @GeneratedValue
    private long id;
    @Column(unique = true)
    private String accountId;
    private float balance;
    @ElementCollection
    private List<AccountEntityOperation> operations;

    BankAccount convertToDomain() {
        List<AccountOperation> accountOperations = new ArrayList<>();
        if (null != this.operations && !this.operations.isEmpty()) {
            accountOperations = this.operations.stream().map(AccountEntityOperation::convertToDomain).collect(Collectors.toList());
        }

        return BankAccount.builder().accountId(accountId).accountBalance(balance).operations(accountOperations).build();
    }

    public static Optional<AccountEntity> convertFromDomain(final BankAccount account) {
        List<AccountEntityOperation> entityOperations = new ArrayList<>();
        if (null == account) {
            return Optional.empty();
        }
        if (null != account.getOperations() && !account.getOperations().isEmpty()) {
            entityOperations = account.getOperations().stream().map(AccountEntityOperation::convertFromDomain).toList();
        }
        return Optional.of(AccountEntity.builder()
                                        .accountId(account.getAccountId())
                                        .balance(account.getAccountBalance())
                                        .operations(entityOperations)
                                        .build());
    }
}
