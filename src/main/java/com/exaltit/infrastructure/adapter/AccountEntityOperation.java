package com.exaltit.infrastructure.adapter;

import com.exaltit.domain.operation.AccountOperation;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public final class AccountEntityOperation {

    private String operationType;
    private Date operationDate;
    private int amount;
    private float balance;

    public AccountOperation convertToDomain() {
        return new AccountOperation(operationType, operationDate, amount, balance);
    }

    public static AccountEntityOperation convertFromDomain(final AccountOperation accountOperation) {
        return new AccountEntityOperation(accountOperation.getOperationType(), accountOperation.getOperationDate(),
                accountOperation.getAmount(), accountOperation.getBalance()
        );
    }
}
