package com.exaltit.configuration.bean;

import com.exaltit.domain.account.OpenBankAccountService;
import com.exaltit.domain.account.OpenBankAccountUseCase;
import com.exaltit.domain.deposit.MakeADepositService;
import com.exaltit.domain.deposit.MakeADepositUseCase;
import com.exaltit.domain.operation.AddAccountOperationService;
import com.exaltit.domain.operation.AddAccountOperationUseCase;
import com.exaltit.domain.operation.CheckAccountOperationsService;
import com.exaltit.domain.operation.CheckAccountOperationsUseCase;
import com.exaltit.domain.port.BankAccountProviderPort;
import com.exaltit.domain.withdrawal.MakeAWithdrawalService;
import com.exaltit.domain.withdrawal.MakeAWithdrawalUseCase;
import com.exaltit.infrastructure.adapter.AccountRepository;
import com.exaltit.infrastructure.adapter.BankAccountProviderH2Adapter;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Bean Configuration class.
 *
 * @author Tomiche
 */
@Configuration
@AllArgsConstructor
public class BeanConfiguration {

    AccountRepository accountRepository;

    @Bean
    BankAccountProviderPort bankAccountProviderPort() {
        return new BankAccountProviderH2Adapter(accountRepository);
    }

    @Bean
    AddAccountOperationUseCase addAccountOperationUseCase(final BankAccountProviderPort bankAccountProviderPort) {
        return new AddAccountOperationService(bankAccountProviderPort);
    }

    @Bean
    MakeADepositUseCase makeADepositUseCase(
            final BankAccountProviderPort bankAccountProviderPort,
            final AddAccountOperationUseCase addAccountOperationUseCase
    ) {
        return new MakeADepositService(bankAccountProviderPort, addAccountOperationUseCase);
    }

    @Bean
    MakeAWithdrawalUseCase makeAWithdrawalUseCase(
            final BankAccountProviderPort bankAccountProviderPort,
            final AddAccountOperationUseCase addAccountOperationUseCase
    ) {
        return new MakeAWithdrawalService(bankAccountProviderPort, addAccountOperationUseCase);
    }

    @Bean
    OpenBankAccountUseCase openBankAccountUseCase(
            final BankAccountProviderPort bankAccountProviderPort,
            final AddAccountOperationUseCase addAccountOperationUseCase
    ) {
        return new OpenBankAccountService(bankAccountProviderPort, addAccountOperationUseCase);
    }

    @Bean
    CheckAccountOperationsUseCase checkAccountOperationsUseCase(final BankAccountProviderPort bankAccountProviderPort) {
        return new CheckAccountOperationsService(bankAccountProviderPort);
    }
}
