package com.exaltit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Main Class For our Bank Account Application.
 *
 * @author Tomiche
 */
@SpringBootApplication
public class BankAccountApplication {

    public static void main(final String[] args) {
        @SuppressWarnings("unused")
        final ApplicationContext ctx = SpringApplication.run(BankAccountApplication.class, args);
    }
}
