# README #

### What is this repository for? ###

Bank account kata Think of your personal bank account experience When in doubt, go for the simplest solution
Requirements

* Deposit and Withdrawal
* Account statement (date, amount, balance)
* Statement printing

## Minimum Requirements ##

* Java 17
* Maven 3.9.4

## How to compile the project and generate Documentation ##

`mvn clean install`

## Start SpringBoot Application ##

`mvn spring-boot:run`

## User Stories ##

### User Story 1 ###

In order to save money

As a bank client

I want to make a deposit in my account

### User Story 2 ###

In order to retrieve some or all of my savings

As a bank client

I want to make a withdrawal from my account

### User Story 3 ###

In order to check my operations

As a bank client

I want to see the history (operation, date, amount, balance) of my operations

## Documentation ##

After the code has been packaged, you can find 2 types of documentation :

### Online Documentation ###

Online documentation is available through Swagger Ui.

Launch the application using `mvn spring-boot:run` and then go to [Swagger Ui](http://localhost:8080/swagger-ui/index.html)

You'll be able to play with the application through this UI, so feel free :)

### Offline Documentation ###

An offline documentation is also generated and can be found here `project_path/target/generated-docs/index.html`.

You just have to open it through the browser of your choice.

For information, the offline doc examples are generated through test JUnit tests.
